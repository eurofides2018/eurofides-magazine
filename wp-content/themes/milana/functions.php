<?php
/**
 * Milana functions
 *
 * @package Milana
 */

/*
 *	@@@ iPanel Path Constant @@@
*/
define( 'MILANA_IPANEL_PATH' , get_template_directory() . '/iPanel/' ); 

/*
 *	@@@ iPanel URI Constant @@@
*/
define( 'MILANA_IPANEL_URI' , get_template_directory_uri() . '/iPanel/' );

/*
 *	@@@ Usage Constant @@@
*/
define( 'MILANA_IPANEL_PLUGIN_USAGE' , false );


/*
 *	@@@ Include iPanel Main File @@@
*/
include_once (MILANA_IPANEL_PATH . 'iPanel.php');

// Get theme options globally
function milana_get_theme_options() {
	if(get_option('MILANA_PANEL')) {
		$theme_options_data = maybe_unserialize(get_option('MILANA_PANEL'));
	} else {
		$theme_options_data = Array();
	}

	return $theme_options_data;
}

$milana_theme_options = milana_get_theme_options();

if (!isset($content_width))
	$content_width = 1140; /* pixels */

if (!function_exists('milana_setup')) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function milana_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Milana, use a find and replace
	 * to change 'milana' to the name of your theme in all the template files
	 */
	load_theme_textdomain('milana', get_template_directory() . '/languages');

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support('automatic-feed-links');

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support('post-thumbnails');

	/**
	 * Enable support for JetPack Infinite Scroll
	 *
	 * @link https://jetpack.me/support/infinite-scroll/
	 */
	add_theme_support( 'infinite-scroll', array(
	    'container' => 'content',
	    'footer' => 'page',
	) );

	/**
	 * Enable support for Title Tag
	 *
	 */
	
	add_theme_support( 'title-tag' );

	/**
	 * Enable support for Logo
	 */
	add_theme_support( 'custom-header', array(
	    'default-image' =>  get_template_directory_uri() . '/img/logo.png',
            'width'         => 260,
            'flex-width'    => true,
            'flex-height'   => false,
            'header-text'   => false,
	));

	/**
	 *	Woocommerce support
	 */
	add_theme_support( 'woocommerce' );

	/**
	 * Change customizer features
	 */
	add_action( 'customize_register', 'milana_theme_customize_register' );
	function milana_theme_customize_register( $wp_customize ) {
		$wp_customize->remove_section( 'colors' );

		$wp_customize->add_setting( 'milana_header_transparent_logo' , array(
		     array ( 'default' => '',
				    'sanitize_callback' => 'esc_url_raw'
				    ),
		    'transport'   => 'refresh',
		) );

		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'milana_header_transparent_logo', array(
		    'label'    => esc_html__( 'Logo for Transparent Header (Light logo)', 'milana' ),
		    'section'  => 'header_image',
		    'settings' => 'milana_header_transparent_logo',
		) ) );
	}

	/**
	 * Theme resize image
	 */
	add_image_size( 'milana-blog-thumb', 1140, 700, true);
	add_image_size( 'milana-blog-thumb-sidebar', 848, 521, true);

	add_image_size( 'milana-blog-thumb-2column', 555, 341, true);
	add_image_size( 'milana-blog-thumb-2column-sidebar', 409, 251, true);
	
	add_image_size( 'milana-blog-thumb-widget', 90, 55, true);

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
            'primary' => esc_html__('Header Menu', 'milana'),
            'top' => esc_html__('Top Menu', 'milana'),
            'footer' => esc_html__('Footer Menu', 'milana'),
	) );
	/*
	* Change excerpt length
	*/
	function milana_new_excerpt_length($length) {
		$milana_theme_options = milana_get_theme_options();

		if(isset($milana_theme_options['post_excerpt_legth'])) {
			$post_excerpt_length = $milana_theme_options['post_excerpt_legth'];
		} else {
			$post_excerpt_length = 18;
		}

		return $post_excerpt_length;
	}
	add_filter('excerpt_length', 'milana_new_excerpt_length');
	/**
	 * Enable support for Post Formats
	 */
	add_theme_support('post-formats', array('aside', 'image', 'gallery', 'video', 'audio', 'quote', 'link', 'status', 'chat'));

}
endif;
add_action('after_setup_theme', 'milana_setup');

/**
 * Enqueue scripts and styles
 */
function milana_scripts() {
	$milana_theme_options = milana_get_theme_options();

	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style( 'milana-fonts', milana_google_fonts_url(), array(), '1.0' );
	wp_enqueue_style('owl-main', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.css');
	wp_enqueue_style('milana-stylesheet', get_stylesheet_uri(), array(), '1.0.2', 'all');
	wp_enqueue_style('milana-responsive', get_template_directory_uri() . '/responsive.css', '1.0.2', 'all');

	if(isset($milana_theme_options['enable_theme_animations']) && $milana_theme_options['enable_theme_animations']) {
		wp_enqueue_style('milana-animations', get_template_directory_uri() . '/css/animations.css');
	}

	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.css');
	wp_enqueue_style('milana-select2', get_template_directory_uri() . '/js/select2/select2.css'); // special version, must be prefixed with theme prefix
	wp_enqueue_style('offcanvasmenu', get_template_directory_uri() . '/css/offcanvasmenu.css');
	wp_enqueue_style('nanoscroller', get_template_directory_uri() . '/css/nanoscroller.css');
	wp_enqueue_style('swiper', get_template_directory_uri() . '/css/idangerous.swiper.css');

	add_thickbox();
	
	// Registering scripts to include it in correct order later
	wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.1.1', true);
	wp_register_script('easing', get_template_directory_uri() . '/js/easing.js', array(), '1.3', true);
	wp_register_script('milana-select2', get_template_directory_uri() . '/js/select2/select2.min.js', array(), '3.5.1', true);  // special version, must be prefixed with theme prefix
	wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array(), '2.0.0', true);
	wp_register_script('nanoscroller', get_template_directory_uri() . '/js/jquery.nanoscroller.min.js', array(), '3.4.0', true);

	// Enqueue scripts in correct order
	wp_enqueue_script('milana-script', get_template_directory_uri() . '/js/template.js', array('jquery', 'bootstrap', 'easing', 'milana-select2', 'owl-carousel', 'nanoscroller'), '1.1', true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

}
add_action('wp_enqueue_scripts', 'milana_scripts');

// Deregister scripts
function milana_dequeue_stylesandscripts() {
	if ( class_exists( 'woocommerce' ) ) {
		wp_dequeue_style( 'select2' );
		wp_deregister_style( 'select2' );
	} 
}
add_action( 'wp_enqueue_scripts', 'milana_dequeue_stylesandscripts', 100 );

/**
 * Enqueue scripts and styles for admin area
 */
function milana_admin_scripts() {
	wp_register_style( 'milana-style-admin', get_template_directory_uri() .'/css/admin.css' );
	wp_enqueue_style( 'milana-style-admin' );
	wp_register_style('milana-font-awesome-admin', get_template_directory_uri() . '/css/font-awesome.css');
	wp_enqueue_style( 'milana-font-awesome-admin' );

	wp_register_script('milana-template-admin', get_template_directory_uri() . '/js/template-admin.js', array(), '1.0', true);
	wp_enqueue_script('milana-template-admin');

}
add_action( 'admin_init', 'milana_admin_scripts' );

function milana_load_wp_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'milana_load_wp_media_files' );

/**
 * Theme Welcome message
 */
function milana_show_admin_notice() {

	$current_screen = get_current_screen();
	$current_user = wp_get_current_user();

	if ( ! get_user_meta($current_user->ID, 'milana_welcome_message_ignore') && ( current_user_can( 'install_plugins' ) && ( $current_screen->id == 'themes' ) ) ):
    ?>
    <div class="notice notice-success is-dismissible updated mgt-welcome-message">
    <div class="mgt-welcome-message-show-steps"><div class="mgt-welcome-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" style="height: 10px;" alt="<?php bloginfo('name'); ?>"></div><p class="about-description" style="display: inline-block;margin-bottom: 0; margin-top:3px;margin-right: 5px;"><?php esc_html_e('Follow this steps to setup your Milana theme within minutes', 'milana'); ?></p> <a class="button button-primary" id="mgt-welcome-message-show-steps"><?php esc_html_e('Show steps', 'milana'); ?></a> <a class="button button-secondary" href="<?php echo esc_url( add_query_arg( 'milana_welcome_message_dismiss', '0' ) );?>"><?php esc_html_e('Hide this message forever', 'milana'); ?></a></div>
    <div class="mgt-welcome-message-steps-wrapper">
    	<h2><?php esc_html_e('Thanks for choosing Milana WordPress theme', 'milana'); ?></h2>
        <p class="about-description"><?php esc_html_e('Follow this steps to setup your website within minutes:', 'milana'); ?></p>
    	<div class="mgt-divider"><a href="<?php echo esc_url( add_query_arg( 'page', 'install-required-plugins', 'themes.php' ) ); ?>" class="button button-primary button-hero"><span class="button-step">1</span><?php esc_html_e('Install required & recommended plugins', 'milana'); ?></a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url( add_query_arg( 'page', 'radium_demo_installer', 'themes.php' ) ); ?>" class="button button-primary button-hero"><span class="button-step">2</span><?php esc_html_e('Use 1-Click Demo Data Import', 'milana'); ?></a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url( add_query_arg( 'page', 'ipanel_MILANA_PANEL', 'themes.php' ) ); ?>" class="button button-primary button-hero"><span class="button-step">3</span><?php esc_html_e('Manage theme options', 'milana'); ?></a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url('http://creanncy.com/go/milana-docs/'); ?>" target="_blank" class="button button-secondary button-hero"><span class="button-step">4</span><?php esc_html_e('Read Theme Documentation Guide', 'milana'); ?></a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url('http://creanncy.com/go/subscribe/'); ?>" target="_blank" class="button button-secondary button-hero"><span class="button-step">5</span><?php esc_html_e('Subscribe to updates', 'milana'); ?></a></div>
    	<div class="mgt-divider"><a href="<?php echo esc_url('http://support.creanncy.com/'); ?>" target="_blank" class="button button-secondary button-hero"><span class="button-step">6</span><?php esc_html_e('Ask for support if something does not work', 'milana'); ?></a></div>
    </div>
    </div>    	         
	<?php
	endif;
}
add_action( 'admin_notices', 'milana_show_admin_notice' );

function milana_welcome_message_dismiss() {
	$current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    /* If user clicks to ignore the notice, add that to their user meta */
    if ( isset($_GET['milana_welcome_message_dismiss']) && '0' == $_GET['milana_welcome_message_dismiss'] ) {
	    add_user_meta($user_id, 'milana_welcome_message_ignore', 'true', true);
	}
}
add_action( 'admin_init', 'milana_welcome_message_dismiss' );

// Set/Get current post details for global usage in templates (post position in loop, etc)
function milana_set_post_details($details) {
	global $milana_post_details;

	$milana_post_details = $details;
}
function milana_get_post_details() {
	global $milana_post_details;

	return $milana_post_details;
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/theme-tags.php';

/**
 * Load theme functions.
 */
require get_template_directory() . '/inc/theme-functions.php';

/**
 * Load theme widgets.
 */
require get_template_directory() . '/inc/theme-widgets.php';

/**
 * Load theme dynamic CSS.
 */
require get_template_directory() . '/inc/theme-css.php';

/**
 * Load theme dynamic JS.
 */
require get_template_directory() . '/inc/theme-js.php';

/**
 * Load theme metaboxes.
 */
require get_template_directory() . '/inc/theme-metaboxes.php';