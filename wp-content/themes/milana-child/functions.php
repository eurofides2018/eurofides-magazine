<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
add_action( 'after_setup_theme', 'parent_theme_setup' );
add_filter( 'eu_favourites_videos', 'home_favourites_videos', 10, 1);
add_filter( 'eu_featured_post', 'home_featured_post', 10, 1);

function theme_enqueue_styles() {
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
    wp_enqueue_style( 'milana-parent-style', get_template_directory_uri() . '/style.css');
    //wp_enqueue_style( 'milana-child-style', get_stylesheet_directory_uri() . '/style.css');

    if (isMobile())
        wp_enqueue_style( 'milana-mobile-style', get_stylesheet_directory_uri() . '/mobile.css');
}

function parent_theme_setup() {
    add_image_size( 'video-blog-thumb', 168, 103, true);
    add_image_size( 'featured-thumb', 701, 430, true);
    add_image_size( 'col3-blog-thumb', 343, 210, true);
    add_image_size( 'col2-blog-thumb', 515, 316, true);
    add_image_size( 'full-article', 880, 541, true);

    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    if ($_SERVER['SERVER_NAME'] == 'localhost') {
        $domainName = "test.ecommerce.eurofides.com/";
    }
    else {
        $domainName = $_SERVER['HTTP_HOST'].'/';
    }
    define( 'CURL_URL', $protocol.$domainName);
}

function curl_output($regex) {
	//URL of targeted site
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
	$url = CURL_URL . "white-page";
	$ch = curl_init();

	// set URL and other appropriate options  
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent );

	// check if is test/stage environment
	if (CURL_TEST_PWD_ENABLED) {
		if (strpos(CURL_URL, CURL_TEST_DOMAIN))
			curl_setopt($ch, CURLOPT_USERPWD, CURL_TEST_USER.':'.CURL_TEST_PWD);
			
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	}

	// grab URL and pass it to the browser  
	$output = curl_exec($ch);

	//Regular expression to excerpt the targeted portion  
	preg_match($regex, $output, $matches);

	$html = $matches[0];

	// close curl resource, and free up system resources  
	curl_close($ch);
	
	return $html;
}

function isMobile() {
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    $match = preg_match('/iPad|android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|iP(hone|od)|iris|kindle|lge |maemo|meego.+mobile|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|(Android.+Mobile)|NOKIA|SymbianOS|N900|BlackBerry|Mobile/s', $userAgent);

    //echo 'Result: ' . $match;
    //echo '<br>useragent: ' . $userAgent;

    if ($match > 0)
        return true;

    return false;
}

function home_favourites_videos($video_posts)
{
    echo '<div id="home-col-video" class="'. (isMobile() ? 'col-xs-12' : 'col-md-4') .'">';
    echo '<h2 style="margin-bottom:20px;">VIDEO PREFERITI</h2>';


    foreach ($video_posts as $post) {
        echo '<div class="video-img" style = "padding:0" >';
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'video-blog-thumb');
        if (has_post_thumbnail($post->ID)) {
            $image_bg = 'background-image: url(' . $image[0] . ');';
        } else {
            $image_bg = '';
        }

        echo '<a href="' . esc_url(get_permalink($post->ID)) . '"><div class="video-post-image" data-style="' . esc_attr($image_bg) . '" style="' . esc_attr($image_bg) . '"><img class="video-post-icon" src="' . get_stylesheet_directory_uri() . '/img/play-icon.png" /></div></a>';

        echo '</div>';

        echo '<div class="video-text">';
        echo '<a href="' . esc_url(get_permalink($post->ID)) . '"><h3>' . esc_html($post->post_title) . '</h3></a>';
        echo '</div>';

        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list(', ', '', $post->ID);

        if ($categories_list) {
            echo '<div class="videopost-categories">';
            printf(esc_html__(' %1$s', 'milana'), $categories_list);
            echo '</div>';
        }

        echo '<div class="clearfix spacerb15"></div>';
    }
    echo '</div>';
}

function home_featured_post($feat_posts)
{
    echo '<div id="featured_post" class="'. (isMobile() ? 'col-xs-12 order-first' : 'col-md-8 order-last') .'">';
    foreach ($feat_posts as $post) {
        $limit = 27;

        $excerpt = explode(' ', get_the_excerpt(), $limit);
        if (count($excerpt) >= $limit) {
            array_pop($excerpt);
            $excerpt = implode(" ", $excerpt) . '...';
        } else {
            $excerpt = implode(" ", $excerpt);
        }

        $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'featured-thumb');

        if (has_post_thumbnail($post->ID)) {
            $image_bg = 'background-image: url(' . $image[0] . ');';
        } else {
            $image_bg = '';
        }

        echo '<div class="milana-post-image" data-style="' . esc_attr($image_bg) . '">';
        echo '<div class="milana-post-title"><a class="milana-post-title-link" href="' . esc_url(get_permalink($post->ID)) . '"><h2>' . esc_html($post->post_title) . '</h2></a>';

        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list(', ');
        if ($categories_list) {
            echo '<div class="featpost-categories">' . sprintf(esc_html__('%1$s', 'milana'), $categories_list) . '</div>';
        }
        echo '</div></div>';
    }
    echo '</div>';
}