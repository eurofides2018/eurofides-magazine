<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Milana
 */

get_header(); 

$milana_theme_options = milana_get_theme_options();

$span_class = 'col-md-12';

// Blog layout
if(isset($milana_theme_options['blog_layout'])) {
	$blog_layout = $milana_theme_options['blog_layout'];
} else {
	$blog_layout = 'layout_default';
}

$temp_query = $wp_query;
?>
<div class="content-block">
	
	<?php milana_welcome_block_show(); ?>

	<div class="page-container container">
		<div class="row">
			<div class="<?php echo esc_attr($span_class);?>">
				<div id="algolia-autocomplete-container"></div>
				
				<?php get_template_part( 'header', 'magazine' ); ?>

				<div class="blog-posts-list clearfix<?php echo esc_attr($blog_masonry_class);?>" id="content">
				<?php 
				$wp_query = $temp_query;
				?>
				<?php if ( have_posts() ) : ?>
				<?php
				
					$videoargs = array(
							'posts_per_page'   => 3,
							'orderby'          => 'date',
							'order'            => 'DESC',
							'tag'	           => 'video',
							'post_type'        => 'post',
							'post_status'      => 'publish',
							'suppress_filters' => 0 
						);

					$video_posts = get_posts( $videoargs );
					
					$featargs = array(
							'posts_per_page'   => 1,
							'orderby'          => 'date',
							'order'            => 'DESC',
							'meta_key'         => '_post_featured_value',
							'meta_value'       => 'on',
							'post_type'        => 'post',
							'post_status'      => 'publish',
							'suppress_filters' => 0 
						);

					$feat_posts = get_posts( $featargs );
					
					$excluded_posts = array_merge($video_posts, $feat_posts);
					$excluded_post_ids = array();
					
					foreach($excluded_posts as $post) {
						array_push($excluded_post_ids, $post->ID);
					}
					
					// get current page we are on. If not set we can assume we are on page 1.
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					// are we on page one?
					if( 1 == $paged && is_front_page() && is_home() ) {
						if (isMobile()) {
							apply_filters( 'eu_featured_post', $feat_posts );
							apply_filters( 'eu_favourites_videos', $video_posts );
						}
						else {
							apply_filters( 'eu_favourites_videos', $video_posts );
							apply_filters( 'eu_featured_post', $feat_posts );
						}
						echo '<div class="clearfix spacerb15"></div>';
					}

					 /* Start the Loop */
					$post_loop_id = 1;
					$args = array(
							'posts_per_page'   => 15,
							'orderby'          => 'date',
							'order'            => 'DESC',
							'post_type'        => 'post',
							'post_status'      => 'publish',
							'post__not_in'	   => $excluded_post_ids,
							'paged'            => get_query_var( 'paged' )
						);
					$wp_query = new WP_Query( $args );
					//$wp_query->set(array( 'post__not_in' => $exclude_posts_id ));
					/*echo 'object_id NOT IN (';
					foreach ($exclude_posts_id as $ex_post_id) {
						set_query_var('p', '-' . $ex_post_id);
						echo $ex_post_id . ', ';
					}
					echo ') ';*/
					?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php //while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<?php
							/* Include the Post-Format-specific template for the content.
							 * If you want to overload this in a child theme then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							$post_loop_details['post_loop_id'] = $post_loop_id;
							$post_loop_details['span_class'] = $span_class;
							milana_set_post_details($post_loop_details);						
							get_template_part( 'content', 'eurofides' );

							$post_loop_id++;
						?>
					
					<?php endwhile; ?>
						
				<?php else : ?>
					<?php get_template_part( 'no-results', 'index' ); ?>
				<?php endif; ?>
				</div>			
				<?php milana_content_nav( 'nav-below' ); ?>			
			</div>
		</div>
	</div>

	<?php if(isset($milana_theme_options['blog_enable_homepage_editorspick_posts']) && $milana_theme_options['blog_enable_homepage_editorspick_posts']): ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php milana_blog_editorspick_posts_show(); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>
<?php get_footer(); ?>