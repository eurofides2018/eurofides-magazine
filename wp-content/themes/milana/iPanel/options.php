<?php
/**
 * SETTINGS TAB
 **/
$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Main Settings', 'milana'),
	'id' => 'main_settings'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "main_settings"
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable theme CSS3 animations', 'milana'),
	"id" => "enable_theme_animations",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Enable colors and background colors fade effects', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable theme images animations on hover', 'milana'),
	"id" => "enable_images_animations",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Enable scale effects on some featured images hover in posts', 'milana')),
	"type" => "checkbox",
);

$ipanel_milana_option[] = array(
	"type" => "EndTab"
);
/**
 * Header TAB
 **/
$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Header', 'milana'),
	'id' => 'header_settings'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "header_settings"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header layout', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Body Background image or pattern', 'milana'),
	"id" => "body_bg_image",
	"field_options" => array(
		"std" => ''
	),
	"desc" => wp_kses_post(__('Upload your site body background image if you want to show it. Remove image to remove background.', 'milana')),
	"type" => "qup",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Body Background image behaviour', 'milana'),
	"id" => "body_bg_style",
	"std" => "cover",
	"options" => array(
		"none" => esc_html__('Default', 'milana'),
		"repeat" => esc_html__('Repeat (for pattern)', 'milana'),
		"cover" => esc_html__('Cover (for large image)', 'milana')
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header Background image or pattern', 'milana'),
	"id" => "header_bg_image",
	"field_options" => array(
		"std" => ''
	),
	"desc" => wp_kses_post(__('Upload your site header background image if you want to show it. Remove image to remove background.', 'milana')),
	"type" => "qup",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header Background image behaviour', 'milana'),
	"id" => "header_bg_style",
	"std" => "cover",
	"options" => array(
		"none" => esc_html__('Default', 'milana'),
		"repeat" => esc_html__('Repeat (for pattern)', 'milana'),
		"cover" => esc_html__('Cover (for large image)', 'milana')
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Disable Header', 'milana'),
	"id" => "disable_header",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This option will disable ALL header (with menu below header, logo, etc). Useful for minimalistic themes with left/right sidebar used to show logo and menu.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header height in pixels', 'milana'),
	"id" => "header_height",
	"std" => "170",
	"desc" => wp_kses_post(__('Default: 170', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Disable top menu', 'milana'),
	"id" => "disable_top_menu",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This option will disable top menu (first menu with social icons and additional links)', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Top menu style', 'milana'),
	"id" => "header_top_menu_style",
	"std" => "menu_black",
	"options" => array(
		"menu_white" => esc_html__('White menu', 'milana'),
		"menu_black" => esc_html__('Black menu', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Disable search in header menu', 'milana'),
	"id" => "disable_top_menu_search",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This option will disable search form in header menu', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Disable text tagline in header', 'milana'),
	"id" => "disable_header_tagline",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This option will disable text tagline in header', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"type" => "htmlpage",
	"name" => '<div class="ipanel-label">
    <label>Logo upload</label>
  </div><div class="ipanel-input">
    You can upload your website logo in <a href="customize.php" target="_blank">WordPress Customizer</a> (in "Header Image" section at the left sidebar).<br/><br/><br/>
  </div>'
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Logo width (px)', 'milana'),
	"id" => "logo_width",
	"std" => "254",
	"desc" => wp_kses_post(__('Default: 254. Upload retina logo (2x size) and input your regular logo width here. For example if your retina logo have 400px width put 200 value here. If you does not use retina logo input regular logo width here (your logo image width).', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Sticky/Fixed top header (with menu, search, social icons)', 'milana'),
	"id" => "enable_sticky_header",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Top Header will be fixed to top if enabled', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show site logo in sticky header', 'milana'),
	"id" => "enable_sticky_header_logo",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Top Header will be fixed to top if enabled', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('MainMenu font decoration', 'milana'),
	"id" => "header_menu_font_decoration",
	"std" => "uppercase",
	"options" => array(
		"uppercase" => esc_html__('Uppercase letters', 'milana'),
		"italic" => esc_html__('Italic letters', 'milana'),
		"none" => esc_html__('None', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('MainMenu font size', 'milana'),
	"id" => "header_menu_font_size",
	"std" => "normalfont",
	"options" => array(
		"largefont" => esc_html__('Large font', 'milana'),
		"normalfont" => esc_html__('Normal font', 'milana')
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('MainMenu font weight', 'milana'),
	"id" => "header_menu_font_weight",
	"std" => "regularfont",
	"options" => array(
		"boldfont" => esc_html__('Bold font', 'milana'),
		"regularfont" => esc_html__('Regular font', 'milana')
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('MainMenu dropdown arrows style for submenus', 'milana'),
	"id" => "header_menu_arrow_style",
	"std" => "downarrow",
	"options" => array(
		"rightarrow" => esc_html__('Right arrow', 'milana'),
		"downarrow" => esc_html__('Down arrow', 'milana'),
		"noarrow" => esc_html__('Disable arrow', 'milana')
	),
	"type" => "select",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Header Logo position', 'milana'),   
	"id" => "header_logo_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'center' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_2.png',
			"label" => esc_html__('Center', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_3.png',
			"label" => esc_html__('Right', 'milana')
		),
	),
	"std" => "center",
	"type" => "image",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header Banner 1 position', 'milana'),   
	"id" => "header_banner_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'center' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_2.png',
			"label" => esc_html__('Center', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_3.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_0.png',
			"label" => esc_html__('Disable', 'milana')
		)
	),
	"std" => "left",
	"desc" => wp_kses_post(__('You can show banner image or some text in your header. Make sure that you use different positions for logo and your banner (for example logo at the left and banner at the right).', 'milana')),
	"type" => "image",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header Banner 1 content', 'milana'),
	"id" => "header_banner_editor",
	"std" => '',
	"desc" => wp_kses_post(__('If you selected Header banner position below you can use any HTML here to show your banner or other content in header.', 'milana')),
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header Banner 2 position', 'milana'),   
	"id" => "header_banner2_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'center' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_2.png',
			"label" => esc_html__('Center', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_3.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/header_block_position_0.png',
			"label" => esc_html__('Disable', 'milana')
		)
	),
	"std" => "right",
	"desc" => wp_kses_post(__('You can show banner image or some text in your header. Make sure that you use different positions for logo and your banner (for example logo at the left and banner at the right).', 'milana')),
	"type" => "image",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Header Banner content', 'milana'),
	"id" => "header_banner2_editor",
	"std" => '',
	"desc" => wp_kses_post(__('If you selected Header banner position below you can use any HTML here to show your banner or other content in header.', 'milana')),
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	
	"name" => esc_html__('Social icons', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('Leave URL fields blank to hide this social icons', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Facebook Page url', 'milana'),
	"id" => "facebook",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Vkontakte page url', 'milana'),
	"id" => "vk",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Twitter Page url', 'milana'),
	"id" => "twitter",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Google+ Page url', 'milana'),
	"id" => "google-plus",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('LinkedIn Page url', 'milana'),
	"id" => "linkedin",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Dribbble Page url', 'milana'),
	"id" => "dribbble",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Behance Page url', 'milana'),
	"id" => "behance",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Instagram Page url', 'milana'),
	"id" => "instagram",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Tumblr page url', 'milana'),
	"id" => "tumblr",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Pinterest page url', 'milana'),
	"id" => "pinterest",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Vimeo page url', 'milana'),
	"id" => "vimeo-square",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('YouTube page url', 'milana'),
	"id" => "youtube",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Skype url', 'milana'),
	"id" => "skype",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Flickr url', 'milana'),
	"id" => "flickr",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('RSS url', 'milana'),
	"id" => "rss",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Deviantart url', 'milana'),
	"id" => "deviantart",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('500px url', 'milana'),
	"id" => "500px",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Etsy url', 'milana'),
	"id" => "etsy",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Telegram url', 'milana'),
	"id" => "telegram",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Odnoklassniki url', 'milana'),
	"id" => "odnoklassniki",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Houzz url', 'milana'),
	"id" => "houzz",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Slack url', 'milana'),
	"id" => "slack",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('QQ url', 'milana'),
	"id" => "qq",
	"std" => "",
	"type" => "text",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	"type" => "EndTab"
);
/**
 * FOOTER TAB
 **/
$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Footer', 'milana'),
	'id' => 'footer_settings'
);
$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "footer_settings"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Instagram in Footer', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Display Instagram Feed in Footer', 'milana'),
	"id" => "footer_instagram_display",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('<a href="https://wordpress.org/plugins/instagram-feed/"" target="_blank">Instagram Feed</a> plugin must be installed and configured by theme documentation before enabling this option.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Footer Instagram title', 'milana'),
	"id" => "footer_instagram_title",
	"std" => "Instagram",
	"desc" => wp_kses_post(__('Leave empty if you don\'t want to show text title.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Footer Instagram subtitle', 'milana'),
	"id" => "footer_instagram_subtitle",
	"std" => "",
	"desc" => wp_kses_post(__('Leave empty if you don\'t want to show text subtitle.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Footer Shortcode Block', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Display Shortcode Block in Footer', 'milana'),
	"id" => "footer_shortcode_display",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Enable block with any shortcode from any plugin on grey background in footer.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Footer Shortcode code', 'milana'),
	"id" => "footer_shortcode_code",
	"std" => '<div class="milana-theme-block">
<div class="col-md-12 text-center">
<h2>Subscribe to newsletter</h2>
Subscribe to our free weekly newsletter and never miss our hand-picked post of the week.
</div>
<div class="col-md-12">[mc4wp_form]</div>
</div>',
	"desc" => wp_kses_post(__('Add shortcode from any plugin that you want to display here (you can combine it with HTML), for example: &#x3C;h1&#x3E;My title&#x3C;/h1&#x3E;&#x3C;div&#x3E;[my_shortcode]&#x3C;/div&#x3E;', 'milana')),
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('General Footer Options', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show "Footer sidebar" only on homepage', 'milana'),
	"id" => "footer_sidebar_1_homepage_only",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Footer Menu', 'milana'),
	"id" => "footer_enable_menu",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Social Icons in Footer', 'milana'),
	"id" => "footer_enable_social",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Footer copyright text', 'milana'),
	"id" => "footer_copyright_editor",
	"std" => "Powered by <a href='http://themeforest.net/user/creanncy/' target='_blank'>Milana - Premium WordPress Theme</a>",
	"field_options" => array(
		'media_buttons' => false
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * SIDEBARS TAB
 **/
$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Sidebars', 'milana'),
	'id' => 'sidebar_settings'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "sidebar_settings"
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Blog page sidebar position', 'milana'),   
	"id" => "blog_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => esc_html__('Disable sidebar', 'milana')
		),
	),
	"std" => "right",
	"type" => "image",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Pages sidebar position', 'milana'),   
	"id" => "page_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => esc_html__('Disable sidebar', 'milana')
		),
	),
	"std" => "disable",
	"type" => "image",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Blog Archive page sidebar position', 'milana'),   
	"id" => "archive_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => esc_html__('Disable sidebar', 'milana')
		),
	),
	"std" => "right",
	"type" => "image",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Blog Search page sidebar position', 'milana'),   
	"id" => "search_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => esc_html__('Disable sidebar', 'milana')
		),
	),
	"std" => "right",
	"type" => "image",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Blog post sidebar position', 'milana'),   
	"id" => "post_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => esc_html__('Disable sidebar', 'milana')
		),
	),
	"std" => "disable",
	"type" => "image",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('WooCommerce pages sidebar position', 'milana'),   
	"id" => "woocommerce_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => esc_html__('Disable sidebar', 'milana')
		),
	),
	"std" => "disable",
	"type" => "image",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('WooCommerce product page sidebar position', 'milana'),   
	"id" => "woocommerce_product_sidebar_position",
	"options" => array(
		'left' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_1.png',
			"label" => esc_html__('Left', 'milana')
		),
		'right' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_2.png',
			"label" => esc_html__('Right', 'milana')
		),
		'disable' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/sidebar_position_3.png',
			"label" => esc_html__('Disable sidebar', 'milana')
		),
	),
	"std" => "disable",
	"type" => "image",
);
$ipanel_milana_option[] = array(
	"type" => "EndTab"
);
/**
 * BLOG TAB
 **/
$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Blog', 'milana'),
	'id' => 'blog_settings'
);
$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "blog_settings"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Main Blog settings', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Blog layout', 'milana'),   
	"id" => "blog_layout",
	"options" => array(
		'layout_default' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/blog_layout_1.png',
			"label" => esc_html__('Default layout', 'milana')
		),
		'layout_2column_design' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/blog_layout_2.png',
			"label" => esc_html__('Show posts in 2 columns', 'milana')
		),
		'layout_list' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/blog_layout_4.png',
			"label" => esc_html__('List with short posts blocks', 'milana')
		),
		'layout_list_advanced' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/blog_layout_5.png',
			"label" => esc_html__('List with short posts and big blocks (every third post)', 'milana')
		),
		'layout_masonry' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/blog_layout_6.png',
			"label" => esc_html__('Masonry layout', 'milana')
		),
		'layout_text' => array(
			"image" => MILANA_IPANEL_URI . 'option-images/blog_layout_7.png',
			"label" => esc_html__('Centered text (Minimnalistic, No images)', 'milana')
		),
	),
	"std" => "layout_default",
	"desc" => wp_kses_post(__('This option will completely change blog listing layout and posts display.', 'milana')),
	"type" => "image",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show blog posts in listing as', 'milana'),
	"id" => "blog_post_loop_type",
	"std" => "content",
	"options" => array(
		"content" => esc_html__('Full content (You will add More tag manually)', 'milana'),
		"excerpt" => esc_html__('Excerpt (Auto crop by words)', 'milana'),
	),
	"desc" => wp_kses_post(__('We recommend you to use Fullwidth layout for Slider Style 3', 'milana')),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Post excerpt length (words)', 'milana'),
	"id" => "post_excerpt_legth",
	"std" => "40",
	"desc" => wp_kses_post(__('Used by WordPress for post shortening. Default: 40', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Disable blog posts loop on main Blog page (Blog homepage)', 'milana'),
	"id" => "blog_disable_posts_loop",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Enable this options if you does not want to show posts on your blog homepage. You can use this to create minimalistic website (you will have just blog slider, welcome blocks and editors picks blocks on homepage.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show author name ("by author") in blog posts', 'milana'),
	"id" => "blog_post_show_author",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show related posts on posts listing page', 'milana'),
	"id" => "blog_list_show_related",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Will show 3 related posts after every post in posts list. Does not available in Masonry layout and 2 column layout.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Post Comments counters in posts blocks', 'milana'),
	"id" => "blog_enable_post_comments_counter",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This option will enable post comments counter display in sliders, popular posts, editor picks, reagular posts blocks.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Post Views counters in posts blocks', 'milana'),
	"id" => "blog_enable_post_views_counter",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This option will enable post views counter display in posts.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider settings', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"field_options" => array(
		"style" => 'alert'
	),
	"name" => wp_kses_post(__('<p>To add your posts to Featured Posts Slider you need to edit your post and set it as featured for slider in Post Settings box.</p>', 'milana')),
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Featured Posts Slider on homepage', 'milana'),
	"id" => "blog_enable_homepage_slider",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can mark posts as featured in post edit screen at the bottom settings box to display it in slider in homepage header.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Featured Posts Slider under header (Transparent header feature)', 'milana'),
	"id" => "blog_enable_homepage_transparent_header",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This feature will make your header transparent and will show it above slider on Homepage. You need to upload light logo version to use this feature.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider width', 'milana'),
	"id" => "blog_homepage_slider_fullwidth",
	"std" => "1",
	"options" => array(
		"1" => esc_html__('Fullwidth', 'milana'),
		"0" => esc_html__('Boxed', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Revolution Slider instead of theme Featured Posts Slider on homepage', 'milana'),
	"id" => "blog_enable_revolution_slider",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You need to create Revolution Slider with alias BLOG_SLIDER that will be used instead of theme slider. <br>IMPORTANT: All theme slider options BELOW will NOT WORK if you enabled Revolution Slider, because you will need to manage ALL slider options in Revolution Slider plugin settings in this case.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider height', 'milana'),
	"id" => "blog_homepage_slider_height",
	"std" => "550",
	"field_options" => array(
		"min" => 250,
		"max" => 800,
		"step" => 5,
		"dimension" => 'px',
		"animation" => true
	),
	"desc" => wp_kses_post(__('Drag to change value. Default: 550px', 'milana')),
	"type" => "slider",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider items per row', 'milana'),
	"id" => "blog_homepage_slider_items",
	"std" => "1",
	"options" => array(
		"2" => "2",
		"1" => "1",
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider post details layout', 'milana'),
	"id" => "blog_homepage_slider_post_details_layout",
	"std" => "horizontal",
	"options" => array(
		"horizontal" => esc_html__('Horizontal', 'milana'),
		"vertical" => esc_html__('Vertical', 'milana'),
		"disable" => esc_html__('Disable Post Details (show as image slider)', 'milana'),
	),
	"desc" => wp_kses_post(__('Select where to show post details (title, description, category, etc) in blog posts inside slider. "Vertical" value will work only if you set 1 items per row in option above.', 'milana')),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Hide slider posts description', 'milana'),
	"id" => "blog_disable_homepage_slider_description",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Check if you want to hide post descriptions in slider.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show slider posts Read More button', 'milana'),
	"id" => "blog_enable_readmore",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Check if you want show Read More button for posts in slider.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Merge slider slides', 'milana'),
	"id" => "blog_enable_homepage_merge_slider",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Check if you want to see different slides widths in one row.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Center active slide', 'milana'),
	"id" => "blog_enable_homepage_center_slide",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Check if you want to see current slide centered and near slides cropped. Work best with slides per row set to 2 or 4.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider autoplay', 'milana'),
	"id" => "blog_homepage_slider_autoplay",
	"std" => "3000",
	"options" => array(
		"10000" => esc_html__('Enable - 10 seconds', 'milana'),
		"5000" => esc_html__('Enable - 5 seconds', 'milana'),
		"3000" => esc_html__('Enable - 3 seconds', 'milana'),
		"2000" => esc_html__('Enable - 2 seconds', 'milana'),
		"1000" => esc_html__('Enable - 1 second', 'milana'),
		"0" => esc_html__('Disable', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider navigation arrows', 'milana'),
	"id" => "blog_homepage_slider_navigation",
	"std" => "1",
	"options" => array(
		"1" => esc_html__('Enable', 'milana'),
		"0" => esc_html__('Disable', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Featured Posts Slider pagination buttons', 'milana'),
	"id" => "blog_homepage_slider_pagination",
	"std" => "false",
	"options" => array(
		"true" => esc_html__('Enable', 'milana'),
		"false" => esc_html__('Disable', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Welcome Block settings', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Welcome Block on homepage', 'milana'),
	"id" => "blog_enable_homepage_welcome_block",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block below your slider or header.', 'milana')),
	"type" => "checkbox",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Homepage Welcome Block content', 'milana'),
	"id" => "blog_homepage_welcome_block_content",
	"std" => '',
	"desc" => wp_kses_post(__('You can use any HTML here to display any content in your welcome block with predefined layout.', 'milana')),
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Popular Posts Carousel settings', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Popular Posts Carousel on homepage', 'milana'),
	"id" => "blog_enable_homepage_popular_slider",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can enable mini slider with popular posts (by views) below your header on homepage.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Popular Posts Block title', 'milana'),
	"id" => "blog_homepage_popular_posts_title",
	"std" => "Popular posts",
	"desc" => wp_kses_post(__('Change default Popular Posts block title. Leave empty to hide title.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Popular Posts Block subtitle', 'milana'),
	"id" => "blog_homepage_popular_posts_subtitle",
	"std" => "",
	"desc" => wp_kses_post(__('Change default Popular Posts block subtitle. Leave empty to hide title.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Popular Posts Carousel posts limit', 'milana'),
	"id" => "blog_homepage_popular_slider_limit",
	"std" => "10",
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Popular Posts Carousel category slug', 'milana'),
	"id" => "blog_homepage_popular_slider_category",
	"std" => "",
	"desc" => wp_kses_post(__('If you want to show popular posts only from some category specify it\'s SLUG here (You can create special category like "Popular" and assing posts to it if you want to show only selected posts). You can see/set category SLUG when you edit category. Leave empty to show posts from all categories.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Popular Posts Slider autoplay', 'milana'),
	"id" => "blog_homepage_popular_slider_autoplay",
	"std" => "3000",
	"options" => array(
		"10000" => esc_html__('Enable - 10 seconds', 'milana'),
		"5000" => esc_html__('Enable - 5 seconds', 'milana'),
		"3000" => esc_html__('Enable - 3 seconds', 'milana'),
		"2000" => esc_html__('Enable - 2 seconds', 'milana'),
		"1000" => esc_html__('Enable - 1 second', 'milana'),
		"0" => esc_html__('Disable', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Editors Picks Homepage Block settings', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"field_options" => array(
		"style" => 'alert'
	),
	"name" => esc_html__('<p>To add your posts to Editors Picks Block you need to edit your post and set it as Editors Pick post in Post Settings box.</p>', 'milana')
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Editors Picks Block on homepage footer', 'milana'),
	"id" => "blog_enable_homepage_editorspick_posts",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Editors Picks Block title', 'milana'),
	"id" => "blog_homepage_editorspick_posts_title",
	"std" => "Editor's Picks",
	"desc" => wp_kses_post(__('Change default Editors Picks Posts block title. Leave empty to hide title.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Editors Picks Block subtitle', 'milana'),
	"id" => "blog_homepage_editorspick_posts_subtitle",
	"std" => "",
	"desc" => wp_kses_post(__('Change default Editors Picks  block subtitle. Leave empty to hide title.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Editors Picks Block layout', 'milana'),
	"id" => "blog_homepage_editorspick_posts_layout",
	"std" => "large",
	"options" => array(
		"masonry" => esc_html__('Masonry', 'milana'),
		"small" => esc_html__('Small Blocks', 'milana'),
		"large" => esc_html__('Large Blocks', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Editors Picks Block posts limit (rows)', 'milana'),
	"id" => "blog_homepage_editorspick_posts_limit",
	"std" => "1",
	"options" => array(
		"1" => esc_html__('One row', 'milana'),
		"2" => esc_html__('Two rows', 'milana'),
	),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Editors Picks Block posts category slug', 'milana'),
	"id" => "blog_homepage_editorspick_posts_category",
	"std" => "",
	"desc" => wp_kses_post(__('If you want to show popular posts only from some category specify it\'s SLUG here (You can create special category like \'Picks\' and assing posts to it if you want to show only selected posts). You can see/set category SLUG when you edit category. Leave empty to show posts from all categories.', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Single Post page settings', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show Post/Page Header Image under header (Transparent header feature)', 'milana'),
	"id" => "blog_enable_post_transparent_header",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This feature will make your header transparent and will show it above post/page header image. You need to upload light logo version to use this feature and assign header image for posts/pages where you want to see this feature.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Use smaller content width on single posts and pages without sidebar', 'milana'),
	"id" => "blog_enable_small_page_width",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('This option add left/right margins on all pages and posts without sidebars to make your content width smaller.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show author info with avatar after single blog post', 'milana'),
	"id" => "blog_enable_author_info",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You need to fill your post author biography details and social links in Users section in WordPress to make this work.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show horizontal post info box at the end of single post', 'milana'),
	"id" => "blog_enable_post_info_bottom",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Show post info box with comments count, views and post share buttons after single post article.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show share links in Single Post header below post title', 'milana'),
	"id" => "blog_enable_singlepost_header_info",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Drop Caps (first big letter) in single post pages', 'milana'),
	"id" => "blog_enable_dropcaps",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show related posts on single post page', 'milana'),
	"id" => "blog_post_show_related",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Hide post featured image on single post page', 'milana'),
	"id" => "blog_post_hide_featured_image",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Enable this if you don\'t want to see featured post image on single post page.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Show prev/next posts navigation links on single post page', 'milana'),
	"id" => "blog_post_navigation",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * FONTS TAB
 **/

$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Fonts', 'milana'),
	'id' => 'font_settings'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "font_settings"
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Headers font', 'milana'),
	"id" => "header_font",
	"desc" => wp_kses_post(__('Font used in headers. Default: Roboto Slab', 'milana')),
	"options" => array(
		"font-sizes" => array(
			" " => esc_html__('Font Size', 'milana'),
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px',
			'28' => '28px',
			'29' => '29px',
			'30' => '30px',
			'31' => '31px',
			'32' => '32px',
			'33' => '33px',
			'34' => '34px',
			'35' => '35px',
			'36' => '36px',
			'37' => '37px',
			'38' => '38px',
			'39' => '39px',
			'40' => '40px',
			'41' => '41px',
			'42' => '42px',
			'43' => '43px',
			'44' => '44px',
			'45' => '45px',
			'46' => '46px',
			'47' => '47px',
			'48' => '48px',
			'49' => '49px',
			'50' => '50px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false
	),
	"std" => array(
		"font-size" => '26',
		"font-family" => 'Roboto Slab'
	),
	"type" => "typography"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Headers font parameters for Google Font', 'milana'),
	"id" => "header_font_options",
	"std" => "400,700",
	"desc" => wp_kses_post(__('You can specify additional Google Fonts paramaters here, for example fonts styles to load. Default: 400,700', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Body font', 'milana'),
	"id" => "body_font",
	"desc" => wp_kses_post(__('Font used in text elements. Default: Roboto Slab', 'milana')),
	"options" => array(
		"font-sizes" => array(
			" " => esc_html__('Font Size', 'milana'),
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false
	),
	"std" => array(
		"font-size" => '16',
		"font-family" => 'Roboto Slab'
	),
	"type" => "typography"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Body font parameters for Google Font', 'milana'),
	"id" => "body_font_options",
	"std" => "400,700",
	"desc" => wp_kses_post(__('You can specify additional Google Fonts paramaters here, for example fonts styles to load. Default: 400,700', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Additional font', 'milana'),
	"id" => "additional_font",
	"desc" => wp_kses_post(__('You can select any additional Google font here and use it in Custom CSS in theme. Default: Roboto Slab', 'milana')),
	"options" => array(
		"font-sizes" => array(
			" " => esc_html__('Font Size', 'milana'),
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px',
			'28' => '28px',
			'29' => '29px',
			'30' => '30px',
			'31' => '31px',
			'32' => '32px',
			'33' => '33px',
			'34' => '34px',
			'35' => '35px',
			'36' => '36px',
			'37' => '37px',
			'38' => '38px',
			'39' => '39px',
			'40' => '40px',
			'41' => '41px',
			'42' => '42px',
			'43' => '43px',
			'44' => '44px',
			'45' => '45px',
			'46' => '46px',
			'47' => '47px',
			'48' => '48px',
			'49' => '49px',
			'50' => '50px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false,
	),
	"std" => array(
		"font-size" => '12',
		"font-family" => 'Roboto Slab'
	),
	"type" => "typography"
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Additional font parameters for Google Font', 'milana'),
	"id" => "additional_font_options",
	"std" => "400,700",
	"desc" => wp_kses_post(__('You can specify additional Google Fonts paramaters here, for example fonts styles to load. Default: 400,700', 'milana')),
	"type" => "text",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Additional font', 'milana'),
	"id" => "additional_font_enable",
	"std" => true,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Uncheck if you don\'t want to use Additional font. This will speed up your site.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => wp_kses_post(__('<span style="color:red">Disable ALL Google Fonts on site</span>', 'milana')),
	"id" => "font_google_disable",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('Use this if you want extra site speed or want to have regular fonts. Arial font will be used with this option.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Regular font (apply if you disabled Google Fonts below)', 'milana'),
	"id" => "font_regular",
	"std" => "Arial",
	"options" => array(
		"Arial" => "Arial",
		"Tahoma" => "Tahoma",
		"Times New Roman" => "Times New Roman",
		"Verdana" => "Verdana",
		"Helvetica" => "Helvetica",
		"Georgia" => "Georgia",
		"Courier New" => "Courier New"
	),
	"desc" => wp_kses_post(__('Use this option if you disabled ALL Google Fonts.', 'milana')),
	"type" => "select",
);
$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * COLORS TAB
 **/

$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Colors & Skins', 'milana'),
	'id' => 'color_settings'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "color_settings",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Predefined color skins', 'milana'),
	"id" => "color_skin_name",
	"std" => "none",
	"options" => array(
		"none" => esc_html__('Use colors specified below', 'milana'),
		"default" => esc_html__('Milana (Default)', 'milana'),
		"black" => esc_html__('Black', 'milana'),
		"grey" => esc_html__('Grey', 'milana'),
		"lightblue" => esc_html__('Light blue', 'milana'),
		"blue" => esc_html__('Blue', 'milana'),
		"red" => esc_html__('Red', 'milana'),
		"green" => esc_html__('Green', 'milana'),
		"orange" => esc_html__('Orange', 'milana'),
		"redorange" => esc_html__('RedOrange', 'milana'),
		"brown" => esc_html__('Brown', 'milana'),
	),
	"desc" => wp_kses_post(__('Select one of predefined skins or use your own colors. If you selected any predefined styles your specified colors below will NOT be applied.', 'milana')),
	"type" => "select",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Body background color', 'milana'),
	"id" => "theme_body_color",
	"std" => "#FFFFFF",
	"desc" => wp_kses_post(__('Used in many theme places, default: #FFFFFF', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Body text color', 'milana'),
	"id" => "theme_text_color",
	"std" => "#000000",
	"desc" => wp_kses_post(__('Body text color, default: #000000', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Theme main color', 'milana'),
	"id" => "theme_main_color",
	"std" => "#d88585",
	"desc" => wp_kses_post(__('Used in many theme places, buttons, links, etc. Default: #d88585', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Header background color', 'milana'),
	"id" => "theme_header_bg_color",
	"std" => "#FFFFFF",
	"desc" => wp_kses_post(__('Default: #FFFFFF', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Category menu background color', 'milana'),
	"id" => "theme_cat_menu_bg_color",
	"std" => "#FFFFFF",
	"desc" => wp_kses_post(__('This background will be used for main menu below header. Default: #FFFFFF', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Footer background color', 'milana'),
	"id" => "theme_footer_color",
	"std" => "#1e1e1e",
	"desc" => wp_kses_post(__('Default: #1e1e1e', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Footer sidebar background color', 'milana'),
	"id" => "theme_footer2_color",
	"std" => "#222222",
	"desc" => wp_kses_post(__('Default: #222222', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Masonry/List blog layout blocks background color', 'milana'),
	"id" => "theme_masonry_bg_color",
	"std" => "#F5F5F5",
	"desc" => wp_kses_post(__('Default: #F5F5F5', 'milana')),
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * BANNERS CODE TAB
 **/
$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Ads management', 'milana'),
	'id' => 'banners_management'
);
$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "banners_management",
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => wp_kses_post(__('<strong>Click ads position name to open settings box.</strong><br>Need new ads position in some specific theme place? <a target="_blank" href="http://support.creanncy.com/">Let our support know</a> where you want to see new ads place and we will add it in next theme update.', 'milana')),
	"field_options" => array(
		"style" => 'alert'
	)
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Site Header Banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed below site Header on all pages.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_below_header",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_below_header_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Site Above Footer Banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed above site footer on all pages.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_above_footer",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_above_footer_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Site Footer Banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed in site footer on all pages.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_footer",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_footer_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner Below Homepage Popular Posts Slider', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed on homepage below Homepage Popular Posts Slider.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_below_homepage_popular_posts",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_below_homepage_popular_posts_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Post Loops Middle Banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed at the middle between posts on all posts listing pages (Homepage, Archives, Search, etc). This banner does not available in Masonry and Two column blog layouts.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_posts_loop_middle",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_posts_loop_middle_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Post Loops Bottom Banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed at the bottom after all posts on posts listing pages (Homepage, Archives, Search, etc).', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_posts_loop_bottom",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_posts_loop_bottom_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Single Blog Post page Top banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed on single blog post page between post content and featured image.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_single_post_top",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_single_post_top_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('Single Blog Post page Bottom banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed on single blog post page after post content.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_single_post_bottom",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_single_post_bottom_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// BANNER OPTIONS
$ipanel_milana_option[] = array(
	"name" => esc_html__('404 Page and Search Nothing Found Banner', 'milana'),   
	"type" => "StartSection",
	"field_options" => array(
		"show" => false // Set true to show items by default.
	)
);
$ipanel_milana_option[] = array(
	"type" => "info",
	"name" => esc_html__('This banner will be displayed on 404 (page not found) and search nothing found pages.', 'milana'),
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Enable Banner', 'milana'),
	"id" => "banner_enable_404",
	"std" => false,
	"field_options" => array(
		"box_label" => ""
	),
	"desc" => wp_kses_post(__('You can display any HTML content in this block to show your advertisement.', 'milana')),
	"type" => "checkbox",
);
$ipanel_milana_option[] = array(
	"name" => esc_html__('Banner HTML code', 'milana'),
	"id" => "banner_404_content",
	"std" => '',
	"field_options" => array(
		'media_buttons' => true
	),
	"type" => "wp_editor",
);
$ipanel_milana_option[] = array(
		"type" => "EndSection"
);
// END BANNERS
$ipanel_milana_option[] = array(
	"type" => "EndTab"
);
/**
 * CUSTOM CODE TAB
 **/

$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Custom CSS/JS', 'milana'),
	'id' => 'custom_code'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "custom_code",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Custom JavaScript code', 'milana'),
	"id" => "custom_js_code",
	"std" => '',
	"field_options" => array(
		"language" => "javascript",
		"line_numbers" => true,
		"autoCloseBrackets" => true,
		"autoCloseTags" => true
	),
	"desc" => wp_kses_post(__('This code will run in header', 'milana')),
	"type" => "code",
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Custom CSS styles', 'milana'),
	"id" => "custom_css_code",
	"std" => '',
	"field_options" => array(
		"language" => "json",
		"line_numbers" => true,
		"autoCloseBrackets" => true,
		"autoCloseTags" => true
	),
	"desc" => wp_kses_post(__('This CSS code will be included in header', 'milana')),
	"type" => "code",
);

$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * DOCUMENTATION TAB
 **/

$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Documentation', 'milana'),
	'id' => 'documentation'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "documentation"
);

function get_plugin_version_number($plugin_name) {
        // If get_plugins() isn't available, require it
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
        // Create the plugins folder and file variables
	$plugin_folder = get_plugins( '/' . $plugin_name );
	$plugin_file = $plugin_name.'.php';
	
	// If the plugin version number is set, return it 
	if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {
		return $plugin_folder[$plugin_file]['Version'];

	} else {
	// Otherwise return null
		return 'Plugin not installed';
	}
}

$ipanel_milana_option[] = array(
	"type" => "htmlpage",
	"name" => '<div class="documentation-icon"><img src="'.esc_url(MILANA_IPANEL_URI).'assets/img/documentation-icon.png" alt="Documentation"/></div><p>We recommend you to read <a href="http://creanncy.com/go/milana-docs/" target="_blank">Theme Documentation</a> before you will start using our theme to building your website. It covers all steps for site configuration, demo content import, theme features usage and more.</p>
<p>If you have face any problems with our theme feel free to use our <a href="http://support.creanncy.com/" target="_blank">Support System</a> to contact us and get help for free.</p>
<a class="button button-primary" href="http://creanncy.com/go/milana-docs/" target="_blank">Theme Documentation</a>
<a class="button button-primary" href="http://support.creanncy.com/" target="_blank">Support System</a><h3>Technical information (paste it to your support ticket):</h3><textarea style="width: 500px; height: 160px;font-size: 12px;">Theme Version: '.wp_get_theme()->get( 'Version' ).'
WordPress Version: '.get_bloginfo( 'version' ).'</textarea>'
);

$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * EXPORT TAB
 **/

$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Export', 'milana'),
	'id' => 'export_settings'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "export_settings"
);
	
$ipanel_milana_option[] = array(
	"name" => esc_html__('Export with Download Possibility', 'milana'),
	"type" => "export",
	"desc" => wp_kses_post(__('Export theme admin panel settings to file.', 'milana'))
);

$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * IMPORT TAB
 **/

$ipanel_milana_tabs[] = array(
	"name" => esc_html__('Import', 'milana'),
	'id' => 'import_settings'
);

$ipanel_milana_option[] = array(
	"type" => "StartTab",
	"id" => "import_settings"
);

$ipanel_milana_option[] = array(
	"name" => esc_html__('Import', 'milana'),
	"type" => "import",
	"desc" => wp_kses_post(__('Select theme options import file or paste options string to import your settings from Export.', 'milana'))
);

$ipanel_milana_option[] = array(
	"type" => "EndTab"
);

/**
 * CONFIGURATION
 **/

$ipanel_configs = array(
	'ID'=> 'MILANA_PANEL', 
	'menu'=> 
		array(
			'submenu' => false,
			'page_title' => esc_html__('Milana Control Panel', 'milana'),
			'menu_title' => esc_html__('Theme Control Panel', 'milana'),
			'capability' => 'manage_options',
			'menu_slug' => 'manage_theme_options',
			'icon_url' => MILANA_IPANEL_URI . 'assets/img/panel-icon.png',
			'position' => 59
		),
	'rtl' => ( function_exists('is_rtl') && is_rtl() ),
	'tabs' => $ipanel_milana_tabs,
	'fields' => $ipanel_milana_option,
	'download_capability' => 'manage_options',
	'live_preview' => false
);

$ipanel_theme_usage = new IPANEL( $ipanel_configs );
	