    (function($){
    $(document).ready(function() {
        
        //MENU
        $('.mainmenu li').hover(
            function(){
                var parentWidth = $(this).width();
                var submenu = $(this).children('ul');
                var parentHeight = $('.dropdown-menu.navbar-nav').outerHeight();
                var pos =  $(this).index() + 1;

                submenu.hide();
                $(this).addClass('open-submenu');
                $('ul.level0', this).addClass('allproductsmenu_' + pos);
                $('ul.level0', this).css({'width' : 1059 - $('.dropdown-menu.navbar-nav').outerWidth() });
				var topSub = 36 * (pos-1) * (-1);

                submenu.show().css({'left' : parentWidth});

                if (submenu.height() < parentHeight) {
                    submenu.css({'height' : parentHeight, 'top' : topSub + 'px'});
                }
            },
            function () {
                $('ul', this).hide();
                $(this).removeClass('open-submenu');
				//alert($(this).parent('ul').parent('nav').attr('class'));
            }
		);

        //toplinks
        $( ".drop-top" ).wrap( "<div class='dropdown customer-dropdown'></div>" );
        $( ".dropdown-menu" ).appendTo( ".customer-dropdown" );

        $( "#checkout-step-login" ).appendTo( "#checkout-step-address-header" );      

		$( "nav.navbar").addClass("bs-prototype-override");
		
		$( "nav.navbar" ).on('hidden.bs.dropdown', function (e) {
			e.currentTarget.show();
		});
		
		$( ".customer-utilities .user-link a" ).click(function() {
			if ($(this).attr("title") == "Login")
				window.open("https://www.eurofides.com/customer/account/login/");
			else
				window.location = $(this).attr('href');
		});
		
		$( ".customer-utilities .wish-items a" ).click(function() { window.open("https://www.eurofides.com/wishlist/"); });
		
		$( ".block-cart.dropdown a" ).click(function() { window.location = $(this).attr('href'); });
		
		if( $('a.linkedin-share').length > 0)  {
			$('.linkedin-share').on('click', milanaChild_linkedinShare);
		}
    });
    })(jQuery);
	
function milanaChild_linkedinShare() {
	var $page_title = encodeURIComponent(jQuery(this).attr('data-title'));
	window.open( 'https://www.linkedin.com/shareArticle?mini=true&url=' + jQuery(this).attr('href') + '&title=' + $page_title, "linkedinWindow", "height=700,width=974,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0")
	return false;
}