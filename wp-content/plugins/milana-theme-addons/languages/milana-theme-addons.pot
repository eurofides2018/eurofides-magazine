msgid ""
msgstr ""
"Project-Id-Version: Milana Theme Addons Plugin\n"
"POT-Creation-Date: 2016-11-28 12:24+0200\n"
"PO-Revision-Date: 2016-11-28 12:24+0200\n"
"Last-Translator: dedalx <dedalx.rus@gmail.com>\n"
"Language-Team: Creanncy <support@creanncy.com>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _e;__;esc_html_e;esc_html__\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../inc/oneclick-demo-import/init.php:183
msgid "Import Revolution sliders ..."
msgstr ""

#: ../inc/oneclick-demo-import/init.php:195
msgid "Slider was not imported due to error."
msgstr ""

#: ../inc/oneclick-demo-import/init.php:197
msgid "Slider imported."
msgstr ""

#: ../inc/oneclick-demo-import/init.php:207
msgid "Revolution slider plugin not installed."
msgstr ""

#: ../inc/oneclick-demo-import/importer/parsers.php:42
#: ../inc/oneclick-demo-import/importer/parsers.php:63
msgid "There was an error when reading this WXR file"
msgstr ""

#: ../inc/oneclick-demo-import/importer/parsers.php:43
msgid ""
"Details are shown above. The importer will now try again with a different "
"parser..."
msgstr ""

#: ../inc/oneclick-demo-import/importer/parsers.php:67
#: ../inc/oneclick-demo-import/importer/parsers.php:72
#: ../inc/oneclick-demo-import/importer/parsers.php:262
#: ../inc/oneclick-demo-import/importer/parsers.php:451
msgid ""
"This does not appear to be a WXR file, missing/invalid WXR version number"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:340
msgid "Import Again"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:344
msgid "Import Demo Data"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:631
msgid "Theme options Import file could not be found. Please try again."
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:689
msgid "Widget Import file could not be found. Please try again."
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:762
msgid "Sidebar does not exist in theme (using Inactive)"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:784
msgid "Site does not support widget"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:807
msgid "Widget already exists"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:856
msgid "Imported"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:859
msgid "Imported to Inactive"
msgstr ""

#: ../inc/oneclick-demo-import/importer/radium-importer.php:866
msgid "No Title"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:142
#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:151
#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:202
#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:206
#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:215
msgid "Sorry, there has been an error."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:143
msgid "The file does not exist, please try again."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:186
msgid "All done."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:186
msgid "Have fun!"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:187
msgid "Remember to update the passwords and roles of imported users."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:207
#, php-format
msgid ""
"The export file could not be found at <code>%s</code>. It is likely that "
"this was caused by a permissions problem."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:223
#, php-format
msgid ""
"This WXR file (version %s) may not be supported by this version of the "
"importer. Please consider updating."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:248
#, php-format
msgid ""
"Failed to import author %s. Their posts will be attributed to the current "
"user."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:274
msgid "Assign Authors"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:275
msgid ""
"To make it easier for you to edit and save the imported content, you may "
"want to reassign the author of the imported item to an existing user of this "
"site. For example, you may want to import all the entries as <code>admin</"
"code>s entries."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:277
#, php-format
msgid ""
"If a new user is created by WordPress, a new password will be randomly "
"generated and the new user&#8217;s role will be set as %s. Manually changing "
"the new user&#8217;s details will be necessary."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:287
msgid "Import Attachments"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:290
msgid "Download and import file attachments"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:307
msgid "Import author:"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:318
msgid "or create new user with login name:"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:321
msgid "as a new user:"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:329
msgid "assign posts to an existing user:"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:331
msgid "or assign posts to an existing user:"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:332
msgid "- Select -"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:382
#, php-format
msgid ""
"Failed to create new user for %s. Their posts will be attributed to the "
"current user."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:431
#, php-format
msgid "Failed to import category %s"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:469
#, php-format
msgid "Failed to import post tag %s"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:535
#, php-format
msgid "Failed to import &#8220;%s&#8221;: Invalid post type %s"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:556
#, php-format
msgid "%s &#8220;%s&#8221; already exists."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:611
#, php-format
msgid "Failed to import %s &#8220;%s&#8221;"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:757
msgid "Menu item skipped due to missing menu slug"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:764
#, php-format
msgid "Menu item skipped due to invalid menu slug: %s"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:926
msgid "Fetching attachments is not enabled"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:939
#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:972
msgid "Invalid file type"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:968
msgid "Empty filename"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:998
#, php-format
msgid ""
"Unable to create directory %s. Is its parent directory writable by the "
"server?"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1004
#, php-format
msgid "Could not write file %s"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1049
msgid "Remote server did not respond"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1055
#, php-format
msgid "Remote server returned error response %1$d %2$s"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1067
msgid "Zero size file downloaded"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1073
#, php-format
msgid "Remote file is too large, limit is %s"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1172
msgid "Import WordPress"
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1186
msgid ""
"Howdy! Upload your WordPress eXtended RSS (WXR) file and we&#8217;ll import "
"the posts, pages, comments, custom fields, categories, and tags into this "
"site."
msgstr ""

#: ../inc/oneclick-demo-import/importer/wordpress-importer.php:1187
msgid "Choose a WXR (.xml) file to upload, then click Upload file and import."
msgstr ""
