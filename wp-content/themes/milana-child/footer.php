<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Milana
 */
?>
<?php 

$milana_theme_options = milana_get_theme_options();

$show_footer_sidebar_1 = true;

if(isset($milana_theme_options['footer_sidebar_1_homepage_only']) && ($milana_theme_options['footer_sidebar_1_homepage_only']) && is_front_page()) {
  $show_footer_sidebar_1 = true;
} 
if(isset($milana_theme_options['footer_sidebar_1_homepage_only']) && ($milana_theme_options['footer_sidebar_1_homepage_only']) && !is_front_page()) {
  $show_footer_sidebar_1 = false;
}
?>

<?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
  <?php if($show_footer_sidebar_1): ?>
  <div class="footer-sidebar-wrapper clearfix">
    <div class="footer-sidebar sidebar container">
      <ul id="footer-sidebar">
        <?php dynamic_sidebar( 'footer-sidebar' ); ?>
      </ul>
    </div>
  </div>
  <?php endif; ?>
<?php endif; ?>
<?php 
// Site Above Footer Banner
milana_banner_show('above_footer'); 
?>

<?php if(is_front_page()): ?>

<?php milana_footer_instagram_show(); ?>

<?php milana_footer_shortcode_show(); ?>

<?php endif; ?>

<?php if (!isMobile()): ?>
<a id="top-link" href="#top"></a>
<?php endif; ?>

<?php
//return Magento Header via curl
echo '<div ' . curl_output('/class="pre-footer">(.*?)<\/footer>/s');
wp_footer();
?>


<!-- Product hit template -->
<script type="text/template" id="autocomplete_products_template">
    <a class="algoliasearch-autocomplete-hit" href="{{url}}">
        {{#thumbnail_url}}
            <div class="thumb"><img src="{{thumbnail_url}}" /></div>
        {{/thumbnail_url}}

        <div class="info">
            {{{_highlightResult.name.value}}}

            <div class="algoliasearch-autocomplete-category">
                {{#categories_without_path}}
                    in                    {{{categories_without_path}}}
                {{/categories_without_path}}

                {{#_highlightResult.color}}
                    {{#_highlightResult.color.value}}
                        <span>
                           {{#categories_without_path}} | {{/categories_without_path}} Color:  {{{_highlightResult.color.value}}}
                        </span>
                    {{/_highlightResult.color.value}}
                {{/_highlightResult.color}}
            </div>

            <div class="algoliasearch-autocomplete-price">
                <span class="after_special {{#price.EUR.default_original_formated}}promotion{{/price.EUR.default_original_formated}}">
                    {{price.EUR.default_formated}}
                </span>

                {{#price.EUR.default_original_formated}}
                    <span class="before_special">
                        {{price.EUR.default_original_formated}}
                    </span>
                {{/price.EUR.default_original_formated}}
            </div>
        </div>
    </a>
</script><!-- Category hit template -->
<script type="text/template" id="autocomplete_categories_template">
    <a class="algoliasearch-autocomplete-hit algolia-clearfix" href="{{url}}">
        {{#image_url}}
            <div class="thumb">
                <img src="{{image_url}}" />
            </div>
        {{/image_url}}

        <div class="info{{^image_url}}-without-thumb{{/image_url}}">

            {{#_highlightResult.path}}
                {{{_highlightResult.path.value}}}
            {{/_highlightResult.path}}
            {{^_highlightResult.path}}
                {{{path}}}
            {{/_highlightResult.path}}

            {{#product_count}}
                <small>({{product_count}})</small>
            {{/product_count}}

        </div>
    </a>
</script><!-- Page hit template -->
<script type="text/template" id="autocomplete_pages_template">
    <a class="algoliasearch-autocomplete-hit algolia-clearfix" href="{{url}}">
        <div class="info-without-thumb">
            {{{_highlightResult.name.value}}}

            {{#content}}
                <div class="details">
                    {{{content}}}
                </div>
            {{/content}}
        </div>
    </a>
</script><!-- Extra attribute hit template -->
<script type="text/template" id="autocomplete_extra_template">
    <a class="algoliasearch-autocomplete-hit" href="{{url}}">
        <div class="info-without-thumb">
            {{{_highlightResult.value.value}}}
        </div>
    </a>
</script><!-- Suggestion hit template -->
<script type="text/template" id="autocomplete_suggestions_template">
    <a class="algoliasearch-autocomplete-hit algolia-clearfix" href="{{url}}">
        <svg xmlns="http://www.w3.org/2000/svg" class="algolia-glass-suggestion magnifying-glass" width="24" height="24" viewBox="0 0 128 128" >
            <g transform="scale(2.5)">
                <path stroke-width="3" d="M19.5 19.582l9.438 9.438"></path>
                <circle stroke-width="3" cx="12" cy="12" r="10.5" fill="none"></circle>
                <path d="M23.646 20.354l-3.293 3.293c-.195.195-.195.512 0 .707l7.293 7.293c.195.195.512.195.707 0l3.293-3.293c.195-.195.195-.512 0-.707l-7.293-7.293c-.195-.195-.512-.195-.707 0z" ></path>
            </g>
        </svg>
        <div class="info-without-thumb">
            {{{_highlightResult.query.value}}}

            {{#category}}
                <span class="text-muted">in</span> <span class="category-tag">{{category}}</span>
            {{/category}}
        </div>
    </a>
</script><!-- General autocomplete menu template -->
<script type="text/template" id="menu-template">
    <div class="autocomplete-wrapper">
        <div class="col9">
            <div class="aa-dataset-products"></div>
        </div>
        <div class="col3">
            <div class="other-sections">
                <div class="aa-dataset-suggestions"></div>
                                    <div class="aa-dataset-0"></div>
                                    <div class="aa-dataset-1"></div>
                                    <div class="aa-dataset-2"></div>
                                    <div class="aa-dataset-3"></div>
                                    <div class="aa-dataset-4"></div>
                                    <div class="aa-dataset-5"></div>
                                    <div class="aa-dataset-6"></div>
                                    <div class="aa-dataset-7"></div>
                                    <div class="aa-dataset-8"></div>
                                    <div class="aa-dataset-9"></div>
                            </div>
        </div>
    </div>
</script>
<!-- Instantsearch wrapper template -->
<script type="text/template" id="instant_wrapper_template">
    {{#findAutocomplete}}
    <div id="algolia-autocomplete-container"></div>
    {{/findAutocomplete}}
    <div id="algolia_instant_selector" class=" with-facets">

        
        <div class="row">
            <div class="col-md-3" id="algolia-left-container">
                <div id="refine-toggle" class="visible-xs visible-sm">+ Refine</div>
                <div class="hidden-xs hidden-sm" id="instant-search-facets-container">
                    <div id="current-refinements"></div>
                </div>
            </div>

            <div class="col-md-9" id="algolia-right-container">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            {{#second_bar}}
                            <div id="instant-search-bar-container">
                                <div id="instant-search-box">
                                    <div class="instant-search-bar-label">
                                        <span class="icon"></span>
                                        <span>Current search</span>
                                    </div>
                                    <div class="instant-search-bar-wrapper">
                                        <label for="instant-search-bar">
                                            Cerca:                                        </label>

                                        <input placeholder="Search for products"
                                               id="instant-search-bar" type="text" autocomplete="off" spellcheck="false"
                                               autocorrect="off" autocapitalize="off"/>

                                        <span class="clear-cross clear-query-instant"></span>
                                    </div>
                                </div>
                            </div>
                            {{/second_bar}}
                        </div>
                    </div>
                </div>
                <div class="row algolia-clearfix">
                    <div>
                        <div class="hits">
                            <div class="infos algolia-clearfix">
                                <div class="pull-left" id="algolia-stats"></div>
                                <div class="pull-right">
                                    <div class="sort-by-label pull-left">
                                        SORT BY                                    </div>
                                    <div class="pull-left" id="algolia-sorts"></div>
                                </div>
                            </div>
                            <div id="instant-search-results-container"></div>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <div id="instant-search-pagination-container"></div>
                </div>
            </div>
        </div>

    </div>
</script>
<!-- Product hit template -->
<script type="text/template" id="instant-hit-template">
    {{#hits}}
        <div class="col-md-4 col-sm-6">
            <div class="result-wrapper">
                <a href="{{url}}" class="result algolia-clearfix">
                    <div class="result-content">
                        <div class="result-thumbnail">
                            {{#image_url}}<img src="{{{ image_url }}}"/>{{/image_url}}
                            {{^image_url}}<span class="no-image"></span>{{/image_url}}
                        </div>
                        <div class="result-sub-content">
                            <h3 class="result-title text-ellipsis">
                                {{{ _highlightResult.name.value }}}
                            </h3>
                            <div class="ratings">
                                <div class="ratings-wrapper">
                                    <div class="ratings-sub-content">
                                        <div class="rating-box">
                                            <div class="rating" style="width:{{rating_summary}}%" width="148" height="148"></div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <div class="price-wrapper">
                                            <div>
                                                <span
                                                    class="after_special {{#price.EUR.default_original_formated}}promotion{{/price.EUR.default_original_formated}}">
                                                    {{price.EUR.default_formated}}
                                                </span>

                                                {{#price.EUR.default_original_formated}}
                                                    <span class="before_special">
                                                        {{price.EUR.default_original_formated}}
                                                    </span>
                                                {{/price.EUR.default_original_formated}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="result-description text-ellipsis">{{{ _highlightResult.description.value }}}</div>

                            {{#isAddToCartEnabled}}
                                {{#in_stock}}
                                    <form action="http://test.ecommerce.eurofides.com//checkout/cart/add/product/{{objectID}}"
                                          method="post">
                                        <input type="hidden" name="form_key"
                                               value="Js0PF1d0Due8iENi"/>
                                        <input type="hidden" name="qty" value="1">
                                        <button type="submit">Aggiungi al carrello</button>
                                    </form>
                                {{/in_stock}}
                            {{/isAddToCartEnabled}}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    {{/hits}}
</script><!-- Search statistics template (used for displaying hits' count and time of query) -->
<script type="text/template" id="instant-stats-template">
    {{#hasOneResult}}
        <strong>1</strong> result found    {{/hasOneResult}}

    {{#hasManyResults}}
        {{^hasNoResults}}
            {{first}}-{{last}} out of        {{/hasNoResults}}
        <strong>{{#helpers.formatNumber}}{{nbHits}}{{/helpers.formatNumber}} results found</strong>
    {{/hasManyResults}}

    in {{seconds}} seconds</script><!-- Refinements lists item template -->
<script type="text/template" id="refinements-lists-item-template">
    <label class="{{cssClasses.label}}">
        <input type="checkbox" class="{{cssClasses.checkbox}}" value="{{name}}" {{#isRefined}}checked{{/isRefined}} />{{name}}
        {{#isRefined}}<span class="cross-circle"></span>{{/isRefined}}
        <span class="{{cssClasses.count}}">
            {{#helpers.formatNumber}}{{count}}{{/helpers.formatNumber}}
        </span>
    </label>
</script><!-- Current refinements template -->
<script type="text/template" id="current-refinements-template">
    <div class="cross-wrapper">
        <span class="clear-cross clear-refinement"></span>
    </div>
    <div class="current-refinement-wrapper">
        {{#label}}
            <span class="current-refinement-label">{{label}}{{^operator}}:{{/operator}}</span>
        {{/label}}

        {{#operator}}
            {{{displayOperator}}}
        {{/operator}}

        {{#exclude}}-{{/exclude}}

        <span class="current-refinement-name">{{name}}</span>
    </div>
</script>    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4jCyTjKSU3cxZApBT9JSpYRYgpcaU55Q";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
<style>
@media (max-width: 600px) {
.zopim {
 display: none !important;
 z-index: 10;
}
}
</style>
</body>
</html>