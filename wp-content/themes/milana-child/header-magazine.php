<div id="magazine-head">
    <?php if (!isMobile()) { ?>
        <div class="col-md-4"><h2><span class="line1">Stili e tendenze</span><br/><span class="line2">dal mondo del packaging</span></h2></div>

        <div class="col-md-4"><h1><a title="Eurofides Magazine" href="<?php echo CURL_URL; ?>magazine/">Magazine</a></h1></div>
        <div class="col-md-4 social-link">
            <a href="https://www.facebook.com/Eurofides?fref=ts" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/facebook.png" />
            </a>
            <a href="https://it.pinterest.com/eurofides/" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pinterest.png" />
            </a>
        </div>
        <div class="clearfix" style="width:1089px;"></div>
    <?php } else { ?>
        <div class="col-xs-12"><h1><a title="Eurofides Magazine" href="<?php echo CURL_URL; ?>magazine/">Magazine</a></h1></div>
        <div class="col-xs-12"><h2>Stili e tendenze dal mondo del packaging</h2></div>
        <div class="clearfix"></div>
    <?php } ?>
</div>

<div class="clearfix" id="blog-menu">
    <?php if (!isMobile()) { ?>
        <div class="col-md-6" style="padding:3px 0 0;">
            <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a href="<?php echo CURL_URL; ?>" title="Vai alla Home Page" itemscope itemtype="http://schema.org/Thing" itemprop="item">
                        <span itemprop="name">Eurofides</span></a>
                    <meta itemprop="position" content="1" />
                </li>
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a href="<?php echo CURL_URL; ?>magazine/" title="Magazine" itemscope itemtype="http://schema.org/Thing" itemprop="item">
                        <span itemprop="name">Magazine</span></a>
                    <meta itemprop="position" content="2" />
                </li>
                <?php if ( is_category() || is_tag() ) :
                    echo ('<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . esc_url( get_category_link( ) ) . '" itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name">' . single_cat_title( '', false ) . '</span></a><meta itemprop="position" content="3" /></li>');
                elseif ( is_singular() ) :
                    $postcategory = get_the_category();
                    echo ('<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . esc_url( get_category_link( $postcategory[0]->term_id ) ) . '" itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name">' . esc_html( $postcategory[0]->name ) . '</span></a><meta itemprop="position" content="3" /></li>');
                    echo ('<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . esc_url( get_the_permalink()) . '" itemscope itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name">' . single_post_title('', false) . '</span></a><meta itemprop="position" content="4" /></li>');
                endif; ?>
            </ol>
        </div>
        <div id="category-menu" class="col-md-6">
            <div class="pull-right">
                <ul style="float: left;">
                    <?php wp_list_categories( array(
                    'hide_empty' => true,
                    'exclude' => array(1),
                    'title_li' => ''
                ) ); ?>
                </ul>
                <div id="video-menu" style="float: left;">
                    <a href="/magazine/tag/video/">Video</a>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div id="category-menu" class="col-xs-6">
            <?php
            $categories = get_categories( array(
                'hide_empty' => true,
                'exclude' => array(1),
            ));
            ?>
            <ul>
                <?php
                foreach($categories as $category) {
                    echo '<li class="cat-item"><a href="' . get_category_link( $category->term_id ) . '"><i class="fa fa-chevron-right"></i> ' . $category->name .'</a></li>';
                }
                ?>
            </ul>
        </div>
        <div id="video-menu" class="col-xs-6">
            <a href="/magazine/tag/video/">Video</a>
        </div>
    <?php } ?>
</div>